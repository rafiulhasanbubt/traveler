//
//  WelcomeViewController.swift
//  Traveler
//
//  Created by rafiul hasan on 5/29/20.
//  Copyright © 2020 rafiul hasan. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {

    @IBOutlet weak var btnSignIn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnSignIn.designButton()
    }
    
    @IBAction func onClickSignUp(_ sender: Any) {
        
    }
    
    @IBAction func onClickSignIn(_ sender: Any) {
        
    }
}

