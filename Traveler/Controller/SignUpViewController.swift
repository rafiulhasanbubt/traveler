//
//  SignUpViewController.swift
//  Travel App
//
//  Created by rafiul hasan on 09/06/19.
//  Copyright © 2019 rafiul hasan. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {

    @IBOutlet weak var txtFullName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBOutlet weak var btnFacebook: UIButton!
    @IBOutlet weak var btnTwitter: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnFacebook.designButton(borderWidth: 0, borderColor: .clear)
        btnTwitter.designButton(borderWidth: 0, borderColor: .clear)
        
        txtFullName.addRightView(image: #imageLiteral(resourceName: "user-icon"))
        txtEmail.addRightView(image: #imageLiteral(resourceName: "email-icon"))
        txtPassword.addRightView(image: #imageLiteral(resourceName: "eye_open"), isSecure: true)
    }


}
