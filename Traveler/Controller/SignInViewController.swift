//
//  SignInViewController.swift
//  Travel App
//
//  Created by rafiul hasan on 09/06/19.
//  Copyright © 2019 rafiul hasan. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController {

    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnForgotPassword: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnForgotPassword.designButton(borderWidth: 0, borderColor: UIColor.clear)
        
        txtEmail.addRightView(image:#imageLiteral(resourceName: "user-icon"))
        txtPassword.addRightView(image:#imageLiteral(resourceName: "eye_closed"), isSecure: true)
    }

    @IBAction func onClickSignIN(_ sender: Any) {
        //UserDefaults.standard.set("LogedIn", forKey: "accessToken")
        changeRoot()
    }
}
