//
//  ButtonDesign.swift
//  Traveler
//
//  Created by rafiul hasan on 5/30/20.
//  Copyright © 2020 rafiul hasan. All rights reserved.
//

import UIKit

extension UIButton {
    func designButton (borderWidth: CGFloat = 1, borderColor: UIColor = #colorLiteral(red: 0.168256253, green: 0.6471060514, blue: 0.9685063958, alpha: 1)){
        layer.cornerRadius = frame.height / 2
        clipsToBounds = true
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor
    }
}
